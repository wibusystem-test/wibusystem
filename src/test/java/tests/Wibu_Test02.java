package tests;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.ConfigReader;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;


public class Wibu_Test02 extends TestBaseRapor {
    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();
    
    
	/*Testfall:
	 * Die Dasboard Startseite sollte angezeigt werden und die Elemente in den Abschnitten von Header, Content und Footer sollten angezeigt werden.
	 * 
	 * Pre-condition
	 * 
	 * Melde  sich mit der URL  mit einem g�ltigen Benutzernamen und Passwort an.
	 * 
	 * Test Data:
	 * "URL: https://wibu.cloud/backend-staging 
        User: 9999/rp-askui
        Passwort: "Haarige2eiten"
        EXPECT:
        Die Elementen werden in der Startseite angezeigt

	 */
	

    @Test
    public void wibuTestCmCloutStarseite() {
        loginPage.userLogin();
        extentTest.info("Login ist aktiv und Willkommen-Text wird angezeigt.");

        Assert.assertTrue(wibuTestPage.TextStartseite.isDisplayed());
        extentTest.info("Startseite wird angezeigt");
        
        Assert.assertTrue(wibuTestPage.iconHerunterladen.isDisplayed());
        extentTest.info("IconHerunterladen wird angezeigt");
        

        ReusableMethods.waitForVisibility(wibuTestPage.appAccount, 15);
        Assert.assertTrue(wibuTestPage.appSettings.isEnabled());
        extentTest.info("Button-Account wird aktiv");
        
        Assert.assertTrue(wibuTestPage.appSettings.isEnabled());
       
        
        Assert.assertTrue(wibuTestPage.appUserHelp.isEnabled());
        extentTest.info("Button-appUserHelp wird aktiv");
      
        String expectedCodeMeterDashboard=ConfigReader.getProperty("expectedCodeMeterDashboard");
        String actualCodeMeterDashboard=wibuTestPage.codeMeterText.getText();
        Assert.assertEquals(actualCodeMeterDashboard,expectedCodeMeterDashboard);
        extentTest.info("Text-(im CodeMeter Cloud Dashboard) wird angezeigt");
       

        Assert.assertTrue(wibuTestPage.datenschutz.isEnabled());
        extentTest.info("Datenschutz ist aktiv");
     
        Assert.assertTrue(wibuTestPage.impressum.isEnabled());
        extentTest.info("Datenschutz ist aktiv");
        Assert.assertTrue(wibuTestPage.kontakt.isEnabled());
        extentTest.info("Datenschutz ist aktiv");

        List<String> expectedList=new ArrayList<>();
        expectedList.add("Startseite");
         expectedList.add("Container");
         expectedList.add("Zertifikate");
         expectedList.add("Upload");
         expectedList.add("Nutzungsberichte");
         for (int i = 0; i < wibuTestPage.hauptMen�Icons.size(); i++) {
            Assert.assertEquals(expectedList.get(i),wibuTestPage.hauptMen�Icons.get(i).getText());

         }
        extentTest.info("Die 5 Haupt-Menn� werden in der linken Seite untereinader angezeigt");
        
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");
        
    }
}
