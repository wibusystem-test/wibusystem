package tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.ConfigReader;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Wibu_Test05 extends TestBaseRapor {
	
	/* Testfall:
	 * Man soll ein neue Container erstellen und download aktiv sein. 
	 * Das Icon von Download wird aktiv sein und auf die Farbe achten und die Farbe mit Mauszeiger dunkler geworden
	 * 
	 * pre-Condition:
	 * Melde  sich mit der URL  mit einem gültigen Benutzernamen und Passwort an.
	 *  
	 *  Test Data:
	 *  URL: https://wibu.cloud/backend-staging 
	 *  User: 9999/rp-askui
	 *  Passwort:"Haarige2eiten"
	 *  
	 *  Expected Ergebnis:
	 * Contaien wird erstellt und erstellete Conatier wird in der Liste angezeigt
	 *  
	 * 
	 */

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void wibuTest2Content() {
        loginPage.userLogin();
        extentTest.info("Login ist aktiv und Willkommen-Text wird angezeigt.");

        //2.2 Content->CmContainer erstellen â€“  (a) einfacher Durchlauf
        ReusableMethods.waitFor(3);
        wibuTestPage.containerErstellenButton.click();
        extentTest.info("Container erstellen wurde angeklickt");

        //wibuTestPage.anzahlContainer.sendKeys(ConfigReader.getProperty("anzahl"));
       // extentTest.info("Die Anzahl wurde eingegeben");
        
        ReusableMethods.waitFor(3);
        wibuTestPage.containerName.sendKeys(ConfigReader.getProperty("name"));
        extentTest.info("Der Name von Container wurde eingegeben");

        wibuTestPage.drobdownCmActID.click();
        extentTest.info("Dropdown wurde angeklickt");
        ReusableMethods.waitFor(2);
        wibuTestPage.drobdownAuswählen.click();
        extentTest.info("Die Auswahl wurde ausgewählt");
        ReusableMethods.waitFor(2);
        wibuTestPage.weiterButton.click();
        extentTest.info("Weiter-Button wurde angeklickt");
        ReusableMethods.waitFor(2);
        wibuTestPage.weiterButton.click();
        extentTest.info("Weiter-Button wurde angeklickt");
        ReusableMethods.waitFor(2);
        wibuTestPage.kleinUploadSymbol.click();
        extentTest.info("Weiter-Button wurde angeklickt");
        ReusableMethods.waitFor(3);
        Assert.assertTrue(wibuTestPage.orangeFarbeSchlüsselSymbol.isDisplayed());
        extentTest.info("Orange-Farbe wurde angezeigt");
        String orangeFarbe=wibuTestPage.orangeFarbeSchlüsselSymbol.getCssValue("col-warning");


        System.out.println("orange Farbe "+orangeFarbe);
        wibuTestPage.anmeldeDaten.click();
        extentTest.info("AnmeldenDaten wurde angeklickt");
        ReusableMethods.waitFor(4);
        wibuTestPage.kleinUploadSymbol.click();
        extentTest.info("upload-Symbol wurde angeklickt");
        String heruntergeladenFarbe=wibuTestPage.kleinUploadSymbol.getCssValue("background-color");
        System.out.println("heruntergeladen Farbe "+heruntergeladenFarbe);
        ReusableMethods.waitFor(5);
        Assert.assertFalse(orangeFarbe.equals(heruntergeladenFarbe));
        wibuTestPage.zurückUbersicht.click();
        extentTest.info("Zurück übersicht wurde angeklickt");
        wibuTestPage.suchbegriff.sendKeys(ConfigReader.getProperty("name")+ Keys.ENTER);
        extentTest.info("Name1 wurde im Suchbegriff eingegeben");

        ReusableMethods.waitFor(5);
        String tabllaText = wibuTestPage.gesehenUbersicht.getText();
        ReusableMethods.waitFor(3);
        String expectedTestName = ConfigReader.getProperty("name");
        System.out.println("=========================================");
        System.out.println("Angesehenen Text =" + tabllaText.toString());
        Assert.assertEquals(tabllaText, expectedTestName);
        extentTest.info("Der Name  wurde in der Tabelle angezeigt");
        
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");




 








    }
}