package tests;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;

public class Wibu_Test04 extends TestBaseRapor {
	
	/* Testfall:
	 * Man soll ein neue Container erstellen und download aktiv sein. 
	 * Das Icon von Download wird aktiv sein und auf die Farbe achten und die Farbe mit Mauszeiger dunkler geworden
	 * 
	 * pre-Condition:
	 * Melde  sich mit der URL  mit einem g�ltigen Benutzernamen und Passwort an.
	 *  
	 *  Test Data:
	 *  URL: https://wibu.cloud/backend-staging 
	 *  User: 9999/rp-askui
	 *  Passwort:"Haarige2eiten"
	 *  
	 *  Expected Ergebnis:
	 * Contaien wird erstellt und erstellete Conatier wird in der Liste angezeigt
	 *  
	 * 
	 */

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void containerSelect(){
        loginPage.userLogin();
        extentTest.info("Login ist aktiv und Willkommen-Text wird angezeigt.");
       String alteFarbe=wibuTestPage.containerAnzeigen.getCssValue("background-color");
        Actions actions=new Actions(Driver.getDriver());
        actions.moveToElement(wibuTestPage.containerAnzeigen).perform();
        String fokusFarbe=wibuTestPage.containerAnzeigen.getCssValue("background-color");
        System.out.println("Alte Farbe: "+alteFarbe);
        System.out.println("Fokus Farbe: "+fokusFarbe);
        Assert.assertFalse(alteFarbe.equals(fokusFarbe));
        extentTest.info(" CmContainer in der Startseite wird die Farbe�nderung verifiziert");


        ReusableMethods.waitFor(3);
        wibuTestPage.startseiteIcon.click();
        Assert.assertFalse(wibuTestPage.zur�ckSymbol.isEnabled());
        extentTest.info(" Zur�ck-Symbol ist inaktiv");
        
        
        ReusableMethods.waitFor(3);
        wibuTestPage.containerAnzeigen.click();
        Assert.assertTrue(wibuTestPage.containerText.isDisplayed());
        extentTest.info(" Container wird  angezeigt");
        
        
        ReusableMethods.waitFor(3);
        Assert.assertTrue(wibuTestPage.zur�ckSymbol.isEnabled());
        wibuTestPage.zur�ckSymbol.click();
        extentTest.info(" Zur�ck-Symbol ist aktiv");

        ReusableMethods.waitFor(3);
        wibuTestPage.updatedateienEinspielen.click();
        extentTest.info("update-dateien einspielen-Seite  ist weitergeleitet");
        Assert.assertTrue(wibuTestPage.updateDateienText.isDisplayed());
        extentTest.info(" Update-Dateien- Text wird  angezeigt");
        
        
        
        Assert.assertTrue(wibuTestPage.zur�ckSymbol.isEnabled());
        extentTest.info(" Zur�ck-Symbol ist aktiv");
        wibuTestPage.zur�ckSymbol.click();
        extentTest.info(" Zur�ck-Symbol wurde angeklickt");
        Assert.assertTrue(wibuTestPage.uploadTitel.isDisplayed());
        extentTest.info(" Upload-Titel wurde angezeigt");
        
        Assert.assertTrue(wibuTestPage.zur�ckSymbol.isEnabled());
        extentTest.info("Zur�ck-Symbol ist aktiv");
        ReusableMethods.waitFor(2);
        wibuTestPage.zur�ckSymbol.click();
        extentTest.info("Zur�ck-Symbol wurde angeklickt");
        Assert.assertFalse(wibuTestPage.zur�ckSymbol.isEnabled());
        extentTest.info("Zur�ck-Symbol ist inaktiv");
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");



    }
}
