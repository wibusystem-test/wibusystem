package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;

import java.awt.datatransfer.StringSelection;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Wibu_Test06 extends TestBaseRapor {
	
	/* Testfall:
	 *Ein Zertifikat wird man hochladen und auf Icon von Upload achten und Updates wird nicht eingespielt.
	 *
	 * pre-Condition:
	 * Melde  sich mit der URL  mit einem gültigen Benutzernamen und Passwort an.
	 *  
	 *  Test Data:
	 *  URL: https://wibu.cloud/backend-staging 
	 *  User: 9999/rp-askui
	 *  Passwort:"Haarige2eiten"
	 *  
	 *  Expected Ergebnis:
	 *Einspilen der Update-Datei ist fehgeschlagen
	 *  
	 * 
	 */

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void contentUploadTest() throws AWTException {
        loginPage.userLogin();
        extentTest.info("Login ist aktiv und Willkommen-Text wird angezeigt.");

        wibuTestPage.iconUpload.click();
        extentTest.info("Upload-icon wurde angeklickt");

        wibuTestPage.dateienAuswählen.click();
        extentTest.info("Dateien-Auswählen wurde angeklickt");

        ReusableMethods.waitFor(5);
        Robot rb = new Robot();
        extentTest.info("Objekt der Roboterklasse wurde erstellt");
        
   
        StringSelection str = new StringSelection("C:\\Users\\admin\\Desktop\\140-.WibuCmRaU"); 
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
        extentTest.info("Dateipfad wurde in der Zwischenablage kopiert");
        
      
 
        rb.keyPress(KeyEvent.VK_CONTROL);
        rb.keyPress(KeyEvent.VK_V);
        extentTest.info("Objekt der Roboterklasse wurde erstellt");
        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);
        System.out.println(wibuTestPage.errorText.getText());
        Assert.assertTrue(wibuTestPage.errorText.isEnabled());
        extentTest.info("Der Errortext wurde verifiziert");
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");





    }


}
