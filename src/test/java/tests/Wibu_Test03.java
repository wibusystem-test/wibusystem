package tests;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;


public class Wibu_Test03 extends TestBaseRapor {
	/* Testfall:
	 * In der Startseite-Men� ist ein Unter-men� und soll man Men�eintarg fokusieren .
	 * Die fokusierente Menu wird  durch Mauszeiger gross und die Farbe ge�ndert
	 * Die Zur�ck-Symbol wird aktiv sein und achtet auf die Farbe�nderung
	 * 
	 * pre-Condition:
	 * Melde  sich mit der URL  mit einem g�ltigen Benutzernamen und Passwort an.
	 *  
	 *  Test Data:
	 *  URL: https://wibu.cloud/backend-staging 
	 *  User: 9999/rp-askui
	 *  Passwort:"Haarige2eiten"
	 *  
	 *  Expected Ergebnis:
	 *  Seletion bleibt erhalten und Menieintrag wird leicht hellt�rkise Farbe geworden
	 *  
	 * 
	 */


    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void HauptMen�Test() {
        loginPage.userLogin();
        extentTest.info("Login ist aktiv und Willkommen-Text wird angezeigt.");

        String alteFarbeStartSeite=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        Actions actions=new Actions(Driver.getDriver());
        ReusableMethods.waitFor(3);
        actions.moveToElement(wibuTestPage.containerMen�).perform();
        String fokusFarbeStartSeite=wibuTestPage.containerMen�.getCssValue("background-color");
        ReusableMethods.waitFor(3);
        System.out.println("Alte Farbe: "+alteFarbeStartSeite);
        System.out.println("Fokus Farbe: "+fokusFarbeStartSeite);
        Assert.assertFalse(alteFarbeStartSeite.equals(fokusFarbeStartSeite));
        extentTest.info(" Startseite im Men� wird die Farbe�nderung verifiziert");
        
        
//a testi
       String alteFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        ReusableMethods.waitFor(3);
        actions.moveToElement(wibuTestPage.zertifikatFarbe).perform();
        String fokusFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        ReusableMethods.waitFor(3);
        System.out.println("Alte Farbe: "+alteFarbe);
        System.out.println("Fokus Farbe: "+fokusFarbe);
        Assert.assertFalse(alteFarbe.equals(fokusFarbe));
        extentTest.info(" Mit Mauszeiger wird die Farbe�nderung verifiziert");


 //b testi
        ReusableMethods.waitFor(5);
        wibuTestPage.zertifikatFarbe.click();
        String geklickteFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        System.out.println("Geklickte Farbe: "+geklickteFarbe);
        Assert.assertFalse(fokusFarbe.equals(geklickteFarbe));
        extentTest.info(" Durch selektiere Men�eintrag wird die Farbe�nderung verifiziert");
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");


















    }
}
