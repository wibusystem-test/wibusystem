package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utilities.ConfigReader;
import utilities.Driver;

public class Wibu_Login_Page {

    public Wibu_Login_Page() {
        PageFactory.initElements(Driver.getDriver(),this);

    }


    @FindBy(id="username")
    public WebElement username;


    @FindBy(id="password")
    public WebElement passwort;


    @FindBy(id="loginbtn")
    public WebElement loginButton;

    @FindBy(xpath = "//div[text()='Willkommen']")       //id="welcome-banner")
    public WebElement testWillkommenText;

    public void userLogin(){
        Driver.getDriver().get(ConfigReader.getProperty("wibu_Url"));
       username.sendKeys(ConfigReader.getProperty("userName"));
       passwort.sendKeys(ConfigReader.getProperty("passwort"));
       loginButton.click();
        Assert.assertTrue(testWillkommenText.isDisplayed());
        System.out.println(testWillkommenText.getText());


    }


}
